/**
 * View Models used by Spring MVC REST controllers.
 */
package com.naroal.web.rest.vm;
